// 3
db.hotel.insertOne({

	"name": "single",
	"accomodates" : 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
})

// 4
db.hotel.insertMany([{

	"name": "double",
	"accomodates" : 3,
	"price": 2000,
	"description": "A room fir for a small family going on a vacation",
	"rooms_available": 5,
	"isAvailable": false
},
{
	"name": "queen",
	"accomodates" : 4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"rooms_available": 15,
	"isAvailable": false
}
])


// 5

db.hotel.find({"name": "double"})


// 6 

db.hotel.updateOne(

	{"name": "queen"},
		{
                    $set: {
                        "rooms_available": 0


			}
		}

	
)

//7
db.hotel.deleteMany({


	"name": "queen"

})

			